describe("Automatizar una suite de pruebas usando Cypress.io", ()=>{
    //Tarea Nro. 3
    //Automatizar un caso de prueba sencillo usando Cypress
    //Autor: Guido Alvaro Huanca Cori
    it("Validar pagina de inicio", ()=>{
        cy.visit('https://opensource-demo.orangehrmlive.com/')
    })
    it("Validar logo Orange HRM", ()=>{
        //Validar logo de la parte superior siempre sea visible
        cy.get('#divLogo > img').should("be.visible")
    })
    it("Validar campo username", ()=>{
        //Validar que el campo username (txtUsername) sea visible
        cy.get('#txtUsername').should("be.visible")
    })
    it("Validar texto OrangeHRM este en la version de la página", ()=>{
        //Validar que en la parte inferior exista una etiqueta con un texto OrangeHRM que diga la version de la página
        cy.get('#footer > :nth-child(1)').contains("OrangeHRM ")
    })
    it("Validar que el boton login sea visible", ()=>{
        //Validar que el boton login sea visible
        cy.get('#btnLogin').should("be.visible")
        //cy.get('#btnLogin').contains("LOGIN")
    })
})