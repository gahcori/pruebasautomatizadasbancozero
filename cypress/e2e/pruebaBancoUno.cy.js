describe("Conjunto de pruebas Banco Zero", ()=>{

    beforeEach(() =>{
        //Ingresar a la pagina 
        cy.visit('http://zero.webappsecurity.com/')
    })
    //Iniciar Pruebas
    it("Validar pagina de inicio", ()=>{
        //cy.visit('http://zero.webappsecurity.com/')
        cy.get('.active > img').should("be.visible")
        cy.get('.active > .custom > h4').contains("Online Banking")
        //cy.get('#signin_button').should("be.visible")
    })
    it("Validar tranferencia de fondos", ()=>{
        //Realizar una transferencia y login con sus validaciones correspondientes
        //Ingresar a la pagina
        //cy.visit('http://zero.webappsecurity.com/')
        //Click en transferencia de fondos
        cy.get('#transfer_funds_link').click()
        //Ingresar el usuario
        cy.get('#user_login').type("username")
        //Ingresar el password
        cy.get('#user_password').type("password")
        //Click en boton ingresar
        cy.get('.btn').click()
        //Seleccionar cuenta de fondo
        cy.get('#tf_fromAccountId').select(0)
        //Seleccionar cuenta destino
        cy.get('#tf_toAccountId').select(1)
        //Ingresar monto a tranferir
        cy.get('#tf_amount').type("250")
        //Ingresamos la descripcion de la transferencia
        cy.get('#tf_description').type("Transferencia de fondos")
        //Click en continuar
        cy.get('#btn_submit').click()
        //Validar si hay el mensaje de enviar
        cy.get('p').should("be.visible")
        //Click en boton enviar
        cy.get('#btn_submit').click()
        //Validar en mensaje de transferencia exitosa
        cy.get('.alert').should("be.visible")
        //cy.get('.alert').should("You successfully submitted your transaction.")

    })
    it("Validar la transferencia del mapa de dinero", ()=>{
        //Ingresar a la pagina
        //cy.visit('http://zero.webappsecurity.com/')
        //Click en registrasrse
        cy.get('#signin_button').click()
        //Ingresar el usuario
        cy.get('#user_login').type("username")
        //Ingresar el password
        cy.get('#user_password').type("password")
        //Click en boton ingresar
        cy.get('.btn').click()
        //Ingresar a my map
        cy.get('#money_map_tab > a').click()
        //Ingresamos al elemento que necesitamos y verificamos que este visible
        cy.get('#ext-sprite-1263').should("be.visible")
        //Click para que no sea visible
        cy.get('#ext-sprite-1175 > tspan').click()
        //Validar que el elemento no sea visible
        cy.get('#ext-sprite-1263').should("not.be.visible")
    })
}) 